This module is a really easy module create to make more easy the seo task.

The idea is to be able to create general tokens with value for 
* Author url
* Publisher url
* Copyright
* Google news keywords


The idea behahind this is to configure one time this module and metatag or path_metatags module, so 
you don't need to fill this fields when you create a content type and you use the values from the tokens.


To use the module, you must fill the form with the values you want to use.
Ones you have fill the values you will have use this tokens:

[variable:tokens4seo_variables_author_url]
[variable:token4seo_variables_google_news_keywords]
[variable:tokens4seo_variables_publisher_url]
[variable:tokens4seo_variables_copyright]
